#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int n, d;
    vector<int> a;

    Solution() {
        scanf("%d%d", &n, &d);
        a.resize(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int ans = 0;
        for (int i = 1; i < n; ++i) {
            int t = 1 - (a[i] - a[i - 1]);
            if (t < 0) t = 0;
            t = (t + d - 1) / d;
            ans += t;
            a[i] += t * d;
        }
        printf("%d\n", ans);
    }
};

int main() {
    Solution();
    return 0;
}
