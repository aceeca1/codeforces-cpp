#include <cstdio>
#include <climits>
#include <vector>
#include <string>
#include <algorithm>
#include <unordered_map>
using namespace std;

struct Record {
    int no, score;
};

int main() {
    int n;
    scanf("%d", &n);
    unordered_map<string, int> no;
    vector<Record> a(n);
    int nameZ = 0;
    for (int i = 0; i < n; ++i) {
        char s[33];
        scanf("%s%d", s, &a[i].score);
        auto p = no.emplace(s, nameZ);
        if (p.second) ++nameZ;
        a[i].no = p.first->second;
    }
    vector<int> b(nameZ);
    for (int i = 0; i < n; ++i) b[a[i].no] += a[i].score;
    int bMax = *max_element(b.begin(), b.end());
    for (int i = 0; i < nameZ; ++i)
        if (b[i] == bMax) b[i] = 0;
        else b[i] = INT_MIN >> 1;
    int i1 = 0;
    for (; i1 < n; ++i1)
        if ((b[a[i1].no] += a[i1].score) >= bMax) break;
    for (auto& i: no)
        if (i.second == a[i1].no) {
            printf("%s\n", i.first.c_str());
            break;
        }
    return 0;
}
