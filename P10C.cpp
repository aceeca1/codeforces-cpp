#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

struct Solution {
    int n;

    int64_t abc9() {
        int64_t ans = 0, a[9];
        a[0] = n / 9;
        for (int i = 1; i < 9; ++i) a[i] = (n + 9 - i) / 9;
        for (int i = 0; i < 9; ++i)
            for (int j = 0; j < 9; ++j)
                ans += a[i] * a[j] * a[i * j % 9];
        return ans;
    }

    int64_t abc() {
        int64_t ans = 0;
        for (int i = 1; i <= n; ++i) ans += n / i;
        return ans;
    }

    Solution() {
        scanf("%d", &n);
        printf("%" PRId64 "\n", abc9() - abc());
    }
};

int main() {
    Solution();
    return 0;
}
