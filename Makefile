objects := $(patsubst %.cpp, %, $(wildcard *.cpp))

all: $(objects)

$(objects): %: %.cpp
	g++ -std=c++17 -g -Og -Wall -Wextra -Werror -Wfatal-errors -o $@ $<

clean:
	rm -rf $(objects)
