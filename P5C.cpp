#include <cstdio>
#include <iostream>
#include <vector>
using namespace std;

int main() {
    string s;
    getline(cin, s);
    int n = s.size(), d = n, len = 0, times = 1;
    vector<int> a((n << 1) + 1, n);
    for (int i = 0; i < n; ++i) {
        if (s[i] == '(') {
            if (a[d] == n) a[d] = i;
            ++d;
        } else a[d--] = n;
        int t = i - a[d] + 1;
        if (t > len) { len = t; times = 1; }
        else if (t == len) ++times;
    }
    printf("%d %d\n", len, len ? times : 1);
    return 0;
}
