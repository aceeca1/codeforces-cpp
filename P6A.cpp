#include <cstdio>
#include <algorithm>
using namespace std;

enum TriStat { TRIANGLE, SEGMENT, IMPOSSIBLE };

TriStat triStat(int* a) {
    if (a[0] + a[1] > a[2]) return TRIANGLE;
    if (a[0] + a[1] == a[2]) return SEGMENT;
    return IMPOSSIBLE;
}

TriStat fourStat(int* a) {
    TriStat tr[4];
    for (int i = 0; i < 4; ++i) {
        int b[3];
        for (int j = 0; j < i; ++j) b[j] = a[j];
        for (int j = i; j < 3; ++j) b[j] = a[j + 1];
        tr[i] = triStat(b);
    }
    for (int i = 0; i < 4; ++i)
        if (tr[i] == TRIANGLE) return TRIANGLE;
    for (int i = 0; i < 4; ++i)
        if (tr[i] == SEGMENT) return SEGMENT;
    return IMPOSSIBLE;
}

int main() {
    int a[4];
    scanf("%d%d%d%d", &a[0], &a[1], &a[2], &a[3]);
    sort(a, a + 4);
    switch (fourStat(a)) {
        case TRIANGLE: printf("TRIANGLE\n"); break;
        case SEGMENT: printf("SEGMENT\n"); break;
        case IMPOSSIBLE: printf("IMPOSSIBLE\n"); break;
    }
    return 0;
}
