#include <cstdio>
#include <vector>
using namespace std;

struct Day {
    int lb, ub;
};

int main() {
    int d, sumTime;
    scanf("%d%d", &d, &sumTime);
    vector<Day> a(d);
    int minTotal = 0, maxTotal = 0;
    for (int i = 0; i < d; ++i) {
        scanf("%d%d", &a[i].lb, &a[i].ub);
        minTotal += a[i].lb;
        maxTotal += a[i].ub;
    }
    if (minTotal <= sumTime && sumTime <= maxTotal) {
        printf("YES\n");
        for (int i = 0; i < d; ++i) {
            a[i].ub -= a[i].lb;
            sumTime -= a[i].lb;
        }
        bool head = true;
        for (int i = 0; i < d; ++i) {
            int some = min(a[i].ub, sumTime);
            sumTime -= some;
            if (!head) printf(" ");
            head = false;
            printf("%d", some + a[i].lb);
        }
        printf("\n");
    } else printf("NO\n");
    return 0;
}
