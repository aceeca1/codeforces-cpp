#include <cstdio>
#include <string>
using namespace std;

struct Tic {
    static constexpr int e[][3] = {
        {0, 1, 2}, {3, 4, 5}, {6, 7, 8},
        {0, 3, 6}, {1, 4, 7}, {2, 5, 8},
        {0, 4, 8}, {2, 4, 6}};
    static constexpr int eZ = sizeof(e) / sizeof(e[0]);
    char b[9];

    bool won(char c) {
        for (int i = 0; i < eZ; ++i)
            if (b[e[i][0]] == c && b[e[i][1]] == c && b[e[i][2]] == c)
                return true;
        return false;
    }

    int count(char c) {
        int ret = 0;
        for (int i = 0; i < 9; ++i) ret += b[i] == c;
        return ret;
    }

    enum Case {ILLEGAL, FIRST_WON, SECOND_WON, DRAW, FIRST_TURN, SECOND_TURN};

    Case check() {
        bool wonX = won('X'), won0 = won('0');
        int countX = count('X'), count0 = count('0'), e = count('.');
        if (wonX && won0) return ILLEGAL;
        if (countX < count0 || countX >= count0 + 2) return ILLEGAL;
        if (countX == count0) {
            if (won0) return SECOND_WON;
            if (wonX) return ILLEGAL;
            return FIRST_TURN;
        } else {
            if (wonX) return FIRST_WON;
            if (won0) return ILLEGAL;
            if (e) return SECOND_TURN;
            return DRAW;
        }
    }

    static string toString(Case c) {
        switch (c) {
            case ILLEGAL: return "illegal";
            case FIRST_WON: return "the first player won";
            case SECOND_WON: return "the second player won";
            case DRAW: return "draw";
            case FIRST_TURN: return "first";
            case SECOND_TURN: return "second";
        }
        return string();
    }
};

constexpr int Tic::e[][3];

Tic readTic() {
    Tic ret;
    for (int i = 0; i < 9; ++i)
        scanf(" %c", &ret.b[i]);
    return ret;
}

int main() {
    Tic a = readTic();
    printf("%s\n", Tic::toString(a.check()).c_str());
    return 0;
}
