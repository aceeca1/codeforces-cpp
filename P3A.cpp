#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;

struct Position {
    char x;
    int y;
};

Position readPos() {
    Position ret;
    scanf(" %c%d", &ret.x, &ret.y);
    return ret;
}

int main() {
    auto p1 = readPos(), p2 = readPos();
    printf("%d\n", max(abs(p1.x - p2.x), abs(p1.y - p2.y)));
    while (p1.x != p2.x || p1.y != p2.y) {
        if (p1.x < p2.x) { printf("R"); ++p1.x; }
        else if (p1.x > p2.x) { printf("L"); --p1.x; }
        if (p1.y < p2.y) { printf("U"); ++p1.y; }
        else if (p1.y > p2.y) { printf("D"); --p1.y; }
        printf("\n");
    }
    return 0;
}
