#include <cstdio>
#include <climits>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

typedef vector<vector<int>> VVInt;

struct FindPathN {
    int len, m;
    VVInt b;
    string path;

    FindPathN(const VVInt& a, int m_): m(m_) {
        b = a;
        for (int i = 0; i < (int)a.size(); ++i)
            for (int j = 0; j < (int)a.size(); ++j) {
                int p = a[i][j], k = 0;
                if (!p) { b[i][j] = 1; continue; }
                while (!(p % m)) { p /= m; ++k; }
                int ans = INT_MAX;
                if (!i && !j) ans = k;
                if (i) {
                    int t = b[i - 1][j] + k;
                    if (t < ans) ans = t;
                }
                if (j) {
                    int t = b[i][j - 1] + k;
                    if (t < ans) ans = t;
                }
                b[i][j] = ans;
            }
        len = b[a.size() - 1][a.size() - 1];
    }

    void calcPath(const VVInt& a) {
        int x = a.size() - 1, y = a.size() - 1;
        while (x || y)
            if (!y || (x && b[x - 1][y] < b[x][y - 1])) {
                --x; path += 'D';
            } else {
                --y; path += 'R';
            }
        reverse(path.begin(), path.end());
    }
};

struct FindPath0 {
    int len, x, y;
    string path;

    FindPath0(const VVInt& a) {
        for (int i = 0; i < (int)a.size(); ++i)
            for (int j = 0; j < (int)a.size(); ++j)
                if (!a[i][j]) { x = i; y = j; len = 1; return; }
        len = INT_MAX;
    }

    void calcPath(const VVInt& a) {
        for (int i = 0; i < x; ++i) path += 'D';
        for (int i = 0; i < (int)a.size() - 1; ++i) path += 'R';
        for (int i = x; i < (int)a.size() - 1; ++i) path += 'D';
    }
};

int main() {
    int n;
    scanf("%d", &n);
    VVInt a(n, vector<int>(n));
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            scanf("%d", &a[i][j]);
    auto a0 = FindPathN(a, 2);
    auto a1 = FindPathN(a, 5);
    auto a2 = FindPath0(a);
    int minLen = a0.len, minLenNo = 0;
    if (a1.len < minLen) { minLen = a1.len; minLenNo = 1; }
    if (a2.len < minLen) { minLen = a2.len; minLenNo = 2; }
    string ans;
    switch (minLenNo) {
        case 0: a0.calcPath(a); ans = a0.path; break;
        case 1: a1.calcPath(a); ans = a1.path; break;
        case 2: a2.calcPath(a); ans = a2.path;
    }
    printf("%d\n%s\n", minLen, ans.c_str());
    return 0;
}
