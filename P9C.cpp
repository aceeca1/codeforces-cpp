#include <cstdio>
#include <iostream>
#include <string>
using namespace std;

struct Solution {
    string s;

    Solution() {
        getline(cin, s);
        int k = 0;
        while (k < (int)s.size() && s[k] <= '1') ++k;
        while (k < (int)s.size()) s[k++] = '1';
        printf("%d\n", (int)stol(s, nullptr, 2));
    }
};

int main() {
    Solution();
    return 0;
}
