#include <cstdio>
#include <string>
#include <vector>
using namespace std;

struct Solution {
    int n, m;
    char c;
    vector<string> a;
    bool e[26]{};

    bool nearC(int x, int y) {
        if (a[x][y] == c || a[x][y] == '.') return false;
        if (x && a[x - 1][y] == c) return true;
        if (y && a[x][y - 1] == c) return true;
        if (x < n - 1 && a[x + 1][y] == c) return true;
        if (y < m - 1 && a[x][y + 1] == c) return true;
        return false;
    }

    Solution() {
        scanf("%d%d %c ", &n, &m, &c);
        for (int i = 0; i < n; ++i) {
            a.emplace_back(m + 1, 0);
            scanf("%s", &a.back()[0]);
            a.back().pop_back();
        }
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j)
                if (nearC(i, j)) e[a[i][j] - 'A'] = true;
        int ans = 0;
        for (int i = 0; i < 26; ++i) ans += e[i];
        printf("%d\n", ans);
    }
};

int main() {
    Solution s;
    return 0;
}
