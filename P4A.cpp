#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    printf("%s\n", n & 1 || n == 2 ? "NO" : "YES");
    return 0;
}
