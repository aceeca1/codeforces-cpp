#include <cstdio>
using namespace std;

struct Solution {
    char a[10];

    bool isCenSym() {
        for (int i = 0; i <= 3; ++i)
            if (a[i] != a[8 - i]) return false;
        return true;
    }

    Solution() {
        scanf("%s%s%s", &a[0], &a[3], &a[6]);
        printf("%s\n", isCenSym() ? "YES" : "NO");
    }
};

int main() {
    Solution();
    return 0;
}
