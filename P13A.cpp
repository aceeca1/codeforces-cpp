#include <cstdio>
#include <utility>
using namespace std;

struct Solution {
    int a;

    int sumOfDigits(int a, int b) {
        int ret = 0;
        while (a) {
            ret += a % b;
            a /= b;
        }
        return ret;
    }

    int gcd(int a1, int a2) {
        while (a2) swap(a1 %= a2, a2);
        return a1;
    }

    Solution() {
        scanf("%d", &a);
        int s = 0;
        for (int i = 2; i < a; ++i) s += sumOfDigits(a, i);
        int d = a - 2, g = gcd(s, d);
        printf("%d/%d\n", s / g, d / g);
    }
};

int main() {
    Solution();
    return 0;
}
