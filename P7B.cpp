#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

struct Solution {
    int t, m, no = 1;
    vector<int> a;

    void alloc() {
        int n, clen = 0;
        scanf("%d", &n);
        for (int i = 0; i < m; ++i) {
            if (a[i]) clen = 0;
            else ++clen;
            if (clen >= n) {
                for (int j = 0; j < n; ++j) a[i - j] = no;
                printf("%d\n", no++);
                return;
            }
        }
        printf("NULL\n");
    }

    void erase() {
        int x;
        scanf("%d", &x);
        if (x <= 0) {
            printf("ILLEGAL_ERASE_ARGUMENT\n");
            return;
        }
        bool success = false;
        for (int i = 0; i < m; ++i)
            if (a[i] == x) { a[i] = 0; success = true; }
        if (!success) printf("ILLEGAL_ERASE_ARGUMENT\n");
    }

    void defragment() {
        int z = remove(a.begin(), a.end(), 0) - a.begin();
        for (int i = z; i < (int)a.size(); ++i) a[i] = 0;
    }

    void process() {
        char s[11];
        scanf("%s", s);
        if (s[0] == 'a') alloc();
        else if (s[0] == 'e') erase();
        else defragment();
    }

    Solution() {
        scanf("%d%d", &t, &m);
        a.resize(m);
        while (t--) process();
    }
};

int main() {
    Solution();
    return 0;
}
