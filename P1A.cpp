#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int n, m, a;
    scanf("%d%d%d", &n, &m, &a);
    printf("%" PRId64 "\n", (n + a - 1) / a * int64_t((m + a - 1) / a));
    return 0;
}
