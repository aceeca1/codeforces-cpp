#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <functional>
using namespace std;

struct Solution {
    int n, m;
    vector<int> a, b;

    int solve() {
        int ans = 0;
        for (int i = 0; i < (int)b.size(); ++i) ans += a[i] * b[i];
        return ans;
    }

    Solution() {
        scanf("%d%d", &n, &m);
        a.resize(n);
        for (int i = 0; i < n; ++i) scanf("%d ", &a[i]);
        unordered_map<string, int> u;
        for (int i = 0; i < m; ++i) {
            string s;
            getline(cin, s);
            ++u[s];
        }
        for (auto& i: u) b.emplace_back(i.second);
        sort(a.begin(), a.end());
        sort(b.begin(), b.end(), greater<int>());
        int ans1 = solve();
        reverse(a.begin(), a.end());
        int ans2 = solve();
        printf("%d %d\n", ans1, ans2);
    }
};

int main() {
    Solution();
    return 0;
}
