#include <cstdio>
#include <vector>
using namespace std;

struct Solution {
    int n;
    vector<int> a;

    Solution() {
        scanf("%d", &n);
        a.resize(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int p = 0, q = n - 1;
        while (p + 1 < q)
            if (a[p] == a[q]) a[p++] = a[q--] = 0;
            else if (a[p] < a[q]) {
                a[q] -= a[p];
                a[p++] = 0;
            } else {
                a[p] -= a[q];
                a[q--] = 0;
            }
        if (p == q) ++q;
        printf("%d %d\n", p + 1, n - q);
    }
};

int main() {
    Solution();
    return 0;
}
