#include <cstdio>
#include <cstring>
#include <string>
#include <vector>
using namespace std;

int main() {
    vector<string> a;
    char s[1002];
    int width = 0;
    while (fgets(s, 1002, stdin)) {
        int z = strlen(s) - 1;
        if (z > width) width = z;
        a.emplace_back(s, s + z);
    }
    for (int i = 0; i < width + 2; ++i) putchar('*');
    printf("\n");
    bool right = false;
    for (auto& ai: a) {
        int e = width - ai.size();
        int e1 = (e + right) >> 1;
        int e2 = e - e1;
        putchar('*');
        for (int i = 0; i < e1; ++i) putchar(' ');
        printf("%s", ai.c_str());
        for (int i = 0; i < e2; ++i) putchar(' ');
        printf("*\n");
        if (e1 != e2) right = !right;
    }
    for (int i = 0; i < width + 2; ++i) putchar('*');
    printf("\n");
    return 0;
}
