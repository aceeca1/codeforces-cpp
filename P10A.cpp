#include <cstdio>
using namespace std;

struct Solution {
    int n, p1, p2, p3, t1, t2;

    int idle(int time) {
        if (time <= t1) return time * p1;
        int ans = t1 * p1;
        time -= t1;
        if (time <= t2) return ans + time * p2;
        ans += t2 * p2;
        time -= t2;
        return ans + time * p3;
    }

    Solution() {
        scanf("%d%d%d%d%d%d", &n, &p1, &p2, &p3, &t1, &t2);
        int le, ri, rlast, ans = 0;
        while (n--) {
            rlast = ri;
            scanf("%d%d", &le, &ri);
            if (ans) ans += idle(le - rlast);
            ans += (ri - le) * p1;
        }
        printf("%d\n", ans);
    }
};

int main() {
    Solution();
    return 0;
}
