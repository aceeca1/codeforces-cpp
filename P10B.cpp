#include <cstdio>
#include <cmath>
#include <climits>
#include <vector>
using namespace std;

struct Cinema {
    int k, c;
    vector<vector<bool>> v;

    Cinema(int k_): k(k_), c(k >> 1), v(k, vector<bool>(k)) {}

    int getD(int row, int c1, int c2) {
        int ans = 0;
        for (int i = c1; i <= c2; ++i) {
            if (v[row][i]) return INT_MAX;
            ans += abs(row - c) + abs(i - c);
        }
        return ans;
    }

    void findPlace(int u, int& row, int& c1, int& c2) {
        int minD = INT_MAX;
        for (int i = 0; i < k; ++i)
            for (int j = 0; ; ++j) {
                int j2 = j + u - 1;
                if (j2 >= k) break;
                int d = getD(i, j, j2);
                if (d < minD) { minD = d; row = i; c1 = j; c2 = j2; }
            }
        if (minD == INT_MAX) row = -1;
        else for (int i = c1; i <= c2; ++i) v[row][i] = true;
    }
};

struct Solution {
    int n, k;

    Solution() {
        scanf("%d%d", &n, &k);
        Cinema c(k);
        while (n--) {
            int u, row, c1 = -1, c2 = -1;
            scanf("%d", &u);
            c.findPlace(u, row, c1, c2);
            if (row == -1) printf("-1\n");
            else printf("%d %d %d\n", row + 1, c1 + 1, c2 + 1);
        }
    }
};

int main() {
    Solution();
    return 0;
}
