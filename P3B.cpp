#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

struct Waterborne {
    int p, no;

    bool operator<(const Waterborne& that) const {
        return p > that.p;
    }
};

void accu(vector<Waterborne>& w) {
    sort(w.begin(), w.end());
    w.emplace_back();
    int a0, a1 = 0, s = 0;
    for (int i = 0; i < (int)w.size(); ++i) {
        a0 = w[i].p;
        w[i].p = s += a1;
        a1 = a0;
    }
}

int main() {
    int n, v;
    scanf("%d%d", &n, &v);
    vector<Waterborne> w1, w2;
    for (int i = 0; i < n; ++i) {
        int t, p;
        scanf("%d%d", &t, &p);
        if (t == 1) w1.emplace_back(Waterborne{p, i});
        else w2.emplace_back(Waterborne{p, i});
    }
    accu(w1); accu(w2);
    int vMax = 0, vMaxNo = -1, vMaxNo1 = -1;
    for (int i = 0; i < (int)w2.size(); ++i) {
        if (i + i > v) break;
        int i1 = min(v - i - i, (int)w1.size() - 1);
        int v = w2[i].p + w1[i1].p;
        if (v > vMax) { vMax = v; vMaxNo = i; vMaxNo1 = i1; }
    }
    printf("%d\n", vMax);
    bool head = true;
    for (int i = 0; i < vMaxNo; ++i) {
        if (!head) printf(" ");
        head = false;
        printf("%d", w2[i].no + 1);
    }
    for (int i = 0; i < vMaxNo1; ++i) {
        if (!head) printf(" ");
        head = false;
        printf("%d", w1[i].no + 1);
    }
    return 0;
}
