#include <cstdio>
#include <cmath>
using namespace std;

struct Solution {
    int x;

    Solution() {
        scanf("%d", &x);
        if (x < 0) x = -x;
        int n = ceil((sqrt((x * 8.0) + 1.0) - 1.0) * 0.5);
        for (; ; ++n) {
            int a = n * (n + 1) >> 1;
            if ((a & 1) == (x & 1)) break;
        }
        printf("%d\n", n);
    }
};

int main() {
    Solution();
    return 0;
}
