#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    int people = 0, ans = 0;
    char s[102];
    while (fgets(s, 102, stdin))
        if (s[0] == '+') ++people;
        else if (s[0] == '-') --people;
        else ans += (strlen(strchr(s, ':') + 1) - 1) * people;
    printf("%d\n", ans);
    return 0;
}
