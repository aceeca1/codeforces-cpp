#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <utility>
using namespace std;

struct ExtendedGcd {
    int64_t a1 = 1, a2 = 0, x1, x2, gcd;

    ExtendedGcd(int64_t o1, int64_t o2): x1(o1), x2(o2) {
        while (o2) {
            int c = o1 / o2;
            swap(a1 -= c * a2, a2);
            swap(o1 -= c * o2, o2);
        }
        gcd = o1;
        if (x2) a2 = (gcd - a1 * x1) / x2;
        if (gcd < 0) {
            gcd = -gcd;
            a1 = -a1;
            a2 = -a2;
        }
    }
};

struct Solution {
    int a, b, c;

    Solution() {
        scanf("%d%d%d", &a, &b, &c);
        ExtendedGcd e(a, b);
        if (c % e.gcd) { printf("-1\n"); return; }
        int ratio = c / -e.gcd;
        printf("%" PRId64 " %" PRId64 "\n", e.a1 * ratio, e.a2 * ratio);
    }
};

int main() {
    Solution();
    return 0;
}
