#include <cstdio>
#include <complex>
#include <vector>
using namespace std;

typedef complex<double> Point;

struct Solution {
    int n, vb, vs;
    vector<int> a;
    Point uni;

    Solution() {
        scanf("%d%d%d", &n, &vb, &vs);
        a.resize(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int ux, uy, iMinT = -1;
        scanf("%d%d", &ux, &uy);
        uni = Point(ux, uy);
        double minT = 1.0 / 0.0;
        for (int i = 1; i < n; ++i) {
            double t1 = double(a[i]) / vb;
            double t2 = abs(uni - double(a[i])) / vs;
            double t = t1 + t2 - i * 1e-9;
            if (t < minT) { minT = t; iMinT = i; }
        }
        printf("%d\n", iMinT + 1);
    }
};

int main() {
    Solution();
    return 0;
}
