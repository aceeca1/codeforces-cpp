#include <cstdio>
#include <cctype>
#include <string>
#include <algorithm>
using namespace std;

bool isRC(const char* s) {
    for (int i = 1; s[i]; ++i)
        if (isdigit(s[i - 1]) && isalpha(s[i])) return true;
    return false;
}

string rc2a(const char* s) {
    int a, b;
    sscanf(s, "R%dC%d", &a, &b);
    string ret;
    while (b) {
        int p = (b - 1) % 26;
        ret += 'A' + p;
        b = (b - 1) / 26;
    }
    reverse(ret.begin(), ret.end());
    ret += to_string(a);
    return move(ret);
}

string a2rc(const char* s) {
    int b = 0, i = 0;
    for (; isalpha(s[i]); ++i)
        b = b * 26 + (s[i] - 'A' + 1);
    int a = strtol(s + i, nullptr, 10);
    string ret("R");
    return ((ret += to_string(a)) += 'C') += to_string(b);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[17];
        scanf("%s", s);
        printf("%s\n", (isRC(s) ? rc2a(s) : a2rc(s)).c_str());
    }
    return 0;
}
