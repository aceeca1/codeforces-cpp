#include <cstdio>
#include <vector>
using namespace std;

struct Point {
    int x, y;
};

int dis(const Point& p1, const Point& p2) {
    int x = p1.x - p2.x, y = p1.y - p2.y;
    return x * x + y * y;
}

struct Solution {
    Point s;
    int n;
    vector<Point> a;
    vector<int> b;

    int len(int p1, int p2) {
        return dis(s, a[p1]) + dis(a[p1], a[p2]) + dis(a[p2], s);
    }

    int visit(int& m, int& a1, int& a2) {
        a1 = 0;
        while (!((m >> a1) & 1)) ++a1;
        int k = m &= ~(1 << a1);
        int v = len(a1, a1) + b[k];
        a2 = a1;
        for (int i = a1 + 1; i < n; ++i) {
            if (!((k >> i) & 1)) continue;
            int k1 = k & ~(1 << i);
            int v1 = len(a1, i) + b[k1];
            if (v1 < v) { v = v1; a2 = i; m = k1; }
        }
        return v;
    }

    Solution() {
        scanf("%d%d%d", &s.x, &s.y, &n);
        a.resize(n);
        for (int i = 0; i < n; ++i)
            scanf("%d%d", &a[i].x, &a[i].y);
        b.resize(1 << n);
        for (int i = 1; i < 1 << n; ++i) {
            int m = i, a1, a2;
            b[i] = visit(m, a1, a2);
        }
        printf("%d\n0", b[(1 << n) - 1]);
        for (int m = (1 << n) - 1; m; ) {
            int a1, a2;
            visit(m, a1, a2);
            if (a1 != a2) printf(" %d", a1 + 1);
            printf(" %d 0", a2 + 1);
        }
        printf("\n");
    }
};

int main() {
    Solution();
    return 0;
}
