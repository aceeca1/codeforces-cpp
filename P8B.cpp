#include <cstdio>
#include <climits>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <deque>
using namespace std;

typedef vector<int> Vertex;
typedef vector<Vertex> Graph;
typedef pair<int, int> Coord;

struct CoordHash {
    size_t operator()(const Coord& c) const {
        constexpr size_t kMul = 0x9ddfea08eb382d69ULL;
        return c.first * kMul + c.second;
    }
};

struct Solution {
    unordered_map<Coord, int, CoordHash> u;
    Graph g;

    void addEdge(int s, int tx, int ty) {
        auto p = u.find(Coord(tx, ty));
        if (p == u.end()) return;
        auto t = p->second;
        g[s].emplace_back(t);
        g[t].emplace_back(s);
    }

    int shortPath(int src, int dst) {
        vector<int> d(g.size(), INT_MAX >> 1);
        d[src] = 0;
        deque<int> dq;
        dq.emplace_back(src);
        while (!dq.empty()) {
            int dqH = dq.front();
            dq.pop_front();
            for (int i: g[dqH]) {
                int t = d[dqH] + 1;
                if (t < d[i]) {
                    d[i] = t;
                    if (!dq.empty() && t < dq.front()) dq.emplace_front(i);
                    else dq.emplace_back(i);
                }
            }
        }
        return d[dst];
    }

    Solution() {
        string s;
        getline(cin, s);
        int x = 0, y = 0;
        u.emplace(Coord(0, 0), 0);
        g.emplace_back();
        for (char i: s) {
            switch (i) {
                case 'R': ++y; break;
                case 'U': --x; break;
                case 'L': --y; break;
                case 'D': ++x;
            }
            auto r = u.emplace(Coord(x, y), g.size());
            if (r.second) g.emplace_back();
        }
        for (auto& i: u) {
            addEdge(i.second, i.first.first + 1, i.first.second);
            addEdge(i.second, i.first.first, i.first.second + 1);
        }
        bool b = shortPath(0, u[Coord(x, y)]) == (int)s.size();
        printf("%s\n", b ? "OK" : "BUG");
    }
};

int main() {
    Solution();
    return 0;
}
