#include <cstdio>
#include <iostream>
#include <string>
#include <algorithm>
#include <utility>
using namespace std;

struct Solution {
    string solve(string&& s) {
        sort(s.begin(), s.end());
        int k = 0;
        while (k < (int)s.size() && s[k] == '0') ++k;
        if (k == (int)s.size()) return s;
        swap(s[0], s[k]);
        return s;
    }

    Solution() {
        string s1, s2;
        getline(cin, s1);
        getline(cin, s2);
        printf("%s\n", solve(move(s1)) == s2 ? "OK" : "WRONG_ANSWER");
    }
};

int main() {
    Solution();
    return 0;
}
