#include <cstdio>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

struct Solution {
    vector<string> a;

    Solution() {
        for (int i = 0; i < 8; ++i) {
            a.emplace_back(9, 0);
            scanf("%s", &a.back()[0]);
            a.back().pop_back();
        }
        int ans = 0, k;
        for (int i = 0; i < 8; ++i)
            if (a[i] == "BBBBBBBB") ++ans;
            else k = i;
        if (ans == 8) { printf("8\n"); return; }
        printf("%d\n", ans + (int)count(a[k].begin(), a[k].end(), 'B'));
    }
};

int main() {
    Solution();
    return 0;
}
