#include <cstdio>
#include <complex>
using namespace std;

typedef complex<double> Point;

void read(Point& p) {
    int x, y;
    scanf("%d%d", &x, &y);
    p = Point(x, y);
}

double cross(const Point& p1, const Point& p2) {
    return p1.real() * p2.imag() - p1.imag() * p2.real();
}

double dot(const Point& p1, const Point& p2) {
    return p1.real() * p2.real() + p1.imag() * p2.imag();
}

struct Segment {
    Point p1, p2;

    Segment rev() const { return Segment{p2, p1}; }

    Point vec() const { return p2 - p1; }

    double cut(const Point& p) const {
        auto v = vec();
        auto v1 = p - p1;
        if (abs(cross(v, v1)) > 1e-12) return -1.0;
        return dot(v, v1) / norm(v);
    }
};

struct Solution {
    Segment s[3];

    bool isA2(Segment s0, Segment s1, Segment s2) {
        if (s1.p1 != s2.p1) return false;
        Point s1v = s1.vec(), s2v = s2.vec();
        if (abs(cross(s1v, s2v)) < 1e-12) return false;
        if (dot(s1v, s2v) < -1e-12) return false;
        if (s1.cut(s0.p2) != -1.0) { swap(s0.p1, s0.p2); }
        double c1 = s1.cut(s0.p1);
        if (c1 < 0.2 - 1e-12 || 0.8 + 1e-12 < c1) return false;
        double c2 = s2.cut(s0.p2);
        if (c2 < 0.2 - 1e-12 || 0.8 + 1e-12 < c2) return false;
        return true;
    }

    bool isA1(const Segment& s0, const Segment& s1, const Segment& s2) {
        auto r1 = s1.rev(), r2 = s2.rev();
        if (isA2(s0, s1, s2)) return true;
        if (isA2(s0, s1, r2)) return true;
        if (isA2(s0, r1, s2)) return true;
        if (isA2(s0, r1, r2)) return true;
        return false;
    }

    bool isA() {
        if (isA1(s[0], s[1], s[2])) return true;
        if (isA1(s[1], s[2], s[0])) return true;
        if (isA1(s[2], s[0], s[1])) return true;
        return false;
    }

    Solution() {
        for (int i = 0; i < 3; ++i) {
            read(s[i].p1);
            read(s[i].p2);
        }
        printf("%s\n", isA() ? "YES" : "NO");
    }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) Solution();
    return 0;
}
