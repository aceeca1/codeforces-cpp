#include <cstdio>
#include <unordered_map>
using namespace std;

int main() {
    unordered_map<string, int> u;
    int n;
    scanf("%d", &n);
    while (n--) {
        char s[33];
        scanf("%s", s);
        auto p = u.emplace(s, 0);
        if (p.second) printf("OK\n");
        else printf("%s%d\n", s, ++p.first->second);
    }
    return 0;
}
