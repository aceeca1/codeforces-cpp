#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

struct Solution {
    string s, a, b;

    bool mat2() {
        auto u = strstr(s.c_str(), a.c_str());
        return u && strstr(u + a.size(), b.c_str());
    }

    Solution() {
        getline(cin, s);
        getline(cin, a);
        getline(cin, b);
        bool b1 = mat2();
        reverse(s.begin(), s.end());
        bool b2 = mat2();
        if (b1) {
            if (b2) printf("both");
            else printf("forward");
        } else {
            if (b2) printf("backward");
            else printf("fantasy");
        }
    }
};

int main() {
    Solution();
    return 0;
}
