#include <cstdio>
#include <cmath>
#include <complex>
#include <vector>
using namespace std;

typedef complex<double> Point;

Point readP() {
    double x, y;
    scanf("%lf%lf", &x, &y);
    return Point(x, y);
}

const double PI = acos(-1);

int main() {
    Point pA = readP(), pB = readP(), pC = readP();
    Point vAB = pB - pA, vBC = pC - pB, vCA = pA - pC;
    double aAB = arg(vAB), aBC = arg(vBC), aCA = arg(vCA);
    double aA = fmod((aCA - aAB + (PI + PI)), PI);
    double aB = fmod((aAB - aBC + (PI + PI)), PI);
    double sinA = sin(aA);
    double d2 = norm(vBC) / (sinA * sinA);
    double sA = aA / PI, sB = aB / PI;
    int n = 3;
    for (;; ++n) {
        double uA = sA * n, uB = sB * n;
        if (abs(uA - round(uA)) > 1e-4) continue;
        if (abs(uB - round(uB)) > 1e-4) continue;
        break;
    }
    printf("%.8f\n", d2 * sin(PI * 2 / n) * n * 0.125);
    return 0;
}
